---
title: "The Cyclicality of Worker Flows: New Evidence from the SIPP"
authors:
- sfujita
- admin
- gramey
# author_notes:
# - "Federal Reserve Bank of Philadelphia"
# - ""
# - "University of California, San Diego"
date: "2007-02-15"
doi: ""

featured: false

# Schedule page publish date (NOT publication's date).
publishDate: "2007-02-15"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: "Philadelphia Fed working papers"

abstract: "Drawing on CPS data, Fujita and Ramey (2006) show that total monthly job loss and hiring among U.S. workers, as well as job loss hazard rates, are strongly countercyclical, while job finding hazard rates are strongly procyclical. They also find that total job loss and job loss hazard rates lead the business cycle, while total hiring and job finding rates trail the cycle. In the current paper we use information from the Survey on Income and Program Participation (SIPP) to reevaluate these findings. SIPP data are used to construct new longitudinally consistent gross flow series for U.S. workers, covering 1983--2003. The results strongly validate the Fujita-Ramey findings, with two important exceptions: (1) total hiring leads the cycle in the SIPP data, and (2) the job loss rate is substantially more volatile than the job finding rate at business cycle frequencies."

# Summary. An optional shortened abstract.
summary: Total monthly job loss and hiring among U.S. workers, as well as job loss hazard rates, are strongly countercyclical, while job finding hazard rates are strongly procyclical.

tags:
- labor markets
- worker flows
- job loss
- hiring
- SIPP

url_pdf: 'publication/cyclicality-worker-flows/wp07-5.pdf'
url_code: ''
url_dataset: ''
url_project: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ""
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---
