---
title: "The Cyclical Behavior of the Price-Cost Markup"
authors:
- admin
- vramey
date: "2020-12-15"
doi: "10.1111/jmcb.12755"

featured: true

# Schedule page publish date (NOT publication's date).
publishDate: "2020-12-01"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "[*Journal of Money, Credit, and Banking*](https://jmcb.osu.edu/), vol. 52, no. S52, December 2020, pp. 319--53"
publication_short: "[*Journal of Money, Credit, and Banking*](https://jmcb.osu.edu/)"

abstract: A countercyclical markup of price over marginal cost is a key transmission mechanism for demand shocks in New Keynesian (NK) models. This paper re-examines the foundation of those models by studying the cyclicality of the price-cost markup in the private economy. We find that how the markup is measured matters for its unconditional cyclicality. Measures of the markup based on the inverse of the labor share are moderately procyclical, but are moderately countercyclical for some generalizations of the production function. NK models predict that the cyclicality of the markup should vary depending on the nature of the shock. Consistent with the NK model, we find that the markup is procyclical conditional on TFP shocks and countercyclical conditional on investment-specific technology shocks. In contrast, we find that the markup increases in response to a positive demand shock. Thus, the transmission mechanism for the effects of demand shocks in sticky-price NK models is not consistent with the data.

# Summary. An optional shortened abstract.
summary: The markup of price over marginal cost increases in response to a positive shock to demand and TFP but decreases in response to an investment-specific technology shock. In contrast, how the markup is measured matters for its unconditional cyclicality.

tags:
- markup
- price-cost markup
- business cycles
- labor share
- technology shock
- New Keynesian

links:
 - name: "PDF (preprint)"
   url: "markupcyc.pdf"
 - name: "Supplemental appendix"
   url: "markupcyc_appendix.pdf"
url_pdf: "https://onlinelibrary.wiley.com/doi/abs/10.1111/jmcb.12755"
url_code: https://gitlab.com/chrisnekarda/markuprep
url_dataset: https://gitlab.com/chrisnekarda/markuprep/-/raw/master/markupcyc_replication.zip?inline=false
url_project: ''
url_slides: ''
url_source: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Placement options: 1 = Full column width, 2 = Out-set, 3 = Screen-width
# Focal point options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
image:
  placement: 1
  caption: "Impulse response of response to an expansionary monetary policy shock"
  focal_point: "Smart"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---

### Previous versions

 - [NBER Working Paper 19099](http://www.nber.org/papers/w19099)