---
title: "Weekly Time Series of the U.S. Labor Market"
authors:
- admin
date: "2008-12-01"

featured: false

# Schedule page publish date (NOT publication's date).
publishDate: "2008-12-01"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

abstract: Data from the Survey of Income and Program Participation (SIPP) are used to create a new data set of U.S. labor market behavior at weekly frequency, including the number of direct employment-to-employment (EE) transitions. The paper documents difficulties encountered creating the weekly series and discusses the strengths and weaknesses of the SIPP data relative to the CPS. Overall the SIPP labor force stocks, gross flows, and cyclical dynamics compare favorably with those from the Current Population Survey (CPS). Abstracting from labor force participation, direct EE transitions account for half of all separations from employment. Although CPS--based estimates of EE flows are nearly twice as high, the CPS overstates EE flows because of time aggregation. Separations to a new job are strongly procyclical while separations to unemployment are strongly countercyclical. The combination yields a nearly acyclical total separation rate.

# Summary. An optional shortened abstract.
summary: The monthly Current Population Survey overstates employment-to-employment transitions because of time aggregation. Separations to a new job are strongly procyclical while separations to unemployment are strongly countercyclical, resulting in an acyclical total separation rate.

tags:
- labor market
- business cycles
- labor market flows
- high-frequency data
- CPS
- SIPP

# links:
#  - name: ""
#    url: ""

# url_pdf: ""
url_code: ""
url_dataset: ""
url_project: ""
url_slides: ""
url_source: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: "Cross-correlations of weekly hazard rates with unemployment rate"
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---

<!-- ### Previous versions -->
