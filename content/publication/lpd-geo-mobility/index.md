---
title: "A Longitudinal Analysis of the Current Population Survey: Assessing the Cyclical Bias of Geographic Mobility"
authors:
- admin
date: "2009-05-27"

featured: false

# Schedule page publish date (NOT publication's date).
publishDate: "2009-05-27"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

abstract: This paper assesses the implications of geographic mobility for the measurement of U.S. labor market dynamics using the Current Population Survey (CPS). Because the CPS does not follow individuals that move, estimates may be biased if the labor market behavior of movers differs systematically from that of nonmovers. I create a new database, the Longitudinal Population Database (LPD), that utilizes all longitudinal information in the CPS to form a panel data set. I use the LPD to identify persons who move and therewith estimate a bound on the bias from geographic mobility. I find that the cyclical bias arising from geographic mobility is small. At business cycle frequencies, the difference between the separation hazard rate calculated from the entire CPS sample and from a subset that are known not to have moved never exceeds 4 percent. There is little effect of mobility on the job finding hazard rate. I conclude that geographic mobility does not significantly affect CPS labor market dynamics.

# Summary. An optional shortened abstract.
summary: Cyclicality in geographic mobility does not significantly affect labor market dynamics measured in the Current Population Survey.

tags:
- labor market
- business cycles
- bias
- labor market flows
- geographic mobility
- CPS
- LPD

# links:
#  - name: ""
#    url: ""

# url_pdf: "publication/timeagg/timeagg.pdf"
url_code: ""
url_dataset: ""
url_project: ""
url_slides: ""
url_source: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ""
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---

<!-- ### Previous versions -->
