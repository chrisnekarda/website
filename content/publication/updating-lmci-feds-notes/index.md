---
title: "Updating the Labor Market Conditions Index"
authors:
- Hess T. Chung
- Bruce Fallick
- admin
- David Ratner
date: "2014-10-01"
doi: "10.17016/2380-7172.0028"

featured: false

# Schedule page publish date (NOT publication's date).
publishDate: "2014-10-01"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated publication name.
publication: "*FEDS Notes*. Washington: Board of Governors of the Federal Reserve System, October 1, 2014."
publication_short: "*FEDS Notes*"

abstract: "**Notice: Monthly updates of the LMCI were discontinued on August 3, 2017.** We will provide updated estimates of the labor market conditions index (LMCI) every month, to be posted sometime after 10:00 a.m. on the first business day following the Bureau of Labor Statistics' monthly Employment Situation report."

# Summary. An optional shortened abstract.
summary: Monthly updates of the LMCI were discontinued on August 3, 2017

tags:
- labor markets
- unemployment
- employment
- unemployment rate
- payroll employment
- dynamic factor model
- LMCI

links:
 - name: "FEDS Note"
   url: "https://www.federalreserve.gov/econresdata/notes/feds-notes/2014/updating-the-labor-market-conditions-index-20141001.html"

url_dataset: "http://www.federalreserve.gov/econresdata/notes/feds-notes/2014/files/lmci_feds.csv"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ""
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---

**Notice:** As of August 3, 2017, updates of the LMCI have been discontinued; the July 7, 2017 vintage is the final estimate from this model.

We decided to stop updating the LMCI because we believe it no longer provides a good summary of changes in U.S. labor market conditions. Specifically, model estimates turned out to be more sensitive to the detrending procedure than we had expected, the measurement of some indicators in recent years has changed in ways that significantly degraded their signal content, and including average hourly earnings as an indicator did not provide a meaningful link between labor market conditions and wage growth.
