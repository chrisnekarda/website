---
title: "The Ins and Outs of Forecasting Unemployment: Using Labor Force Flows to Forecast the Labor Market"
authors:
- rbarnichon
- admin
date: "2012-10-29"
doi: ""

featured: false

# Schedule page publish date (NOT publication's date).
publishDate: "2012-10-29"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "[*Brookings Papers on Economic Activity*](https://www.brookings.edu/project/brookings-papers-on-economic-activity/), no. 2, Fall 2012, pp. 83--131"
publication_short: "[*Brookings Papers on Economic Activity*](https://www.brookings.edu/project/brookings-papers-on-economic-activity/)"

abstract: This paper presents a forecasting model of unemployment based on labor force flows data that, in real time, dramatically outperforms the Survey of Professional Forecasters, historical forecasts from the Federal Reserve Board's Greenbook, and basic time-series models. Our model's forecast has a root-mean-squared error about 30 percent below that of the next-best forecast in the near term and performs especially well surrounding large recessions and cyclical turning points. Further, because our model uses information on labor force flows that is likely not incorporated by other forecasts, a combined forecast including our model's forecast and the SPF forecast yields an improvement over the latter alone of about 35 percent for current-quarter forecasts, and 15 percent for next-quarter forecasts, as well as improvements at longer horizons.

# Summary. An optional shortened abstract.
summary: This paper presents a forecasting model of unemployment based on labor force flows data that, in real time, dramatically outperforms the Survey of Professional Forecasters, historical forecasts from the Federal Reserve Board's Greenbook, and basic time-series models.

tags:
- labor markets
- worker flows
- unemployment
- forecasting
- CPS

links:
url_pdf: "publication/ins-outs-forecasting-unemp/2012b_barnichon.pdf"
url_code: "https://www.brookings.edu/wp-content/uploads/2012/09/barnichon-nekarda-data-programs.zip"
url_dataset: "https://www.brookings.edu/wp-content/uploads/2012/09/barnichon-nekarda-data-programs.zip"
url_project: ""
url_slides: ""
url_source: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ""
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---

- [Conference summary](https://www.brookings.edu/bpea-articles/editors-summary-of-the-brookings-papers-on-economic-activity/)
- [An Introduction to a New Approach to Forecasting Unemployment
](http://www.brookings.edu/blogs/jobs/posts/2012/11/02-jobs-forecasting-explained) -- Includes links to monthly forecast updates by Regis Barnichon

### Media coverage

- 2014-07-31 -- [*WSJ* Real Time Economics](http://blogs.wsj.com/economics/2014/07/31/wall-street-vs-brookings-on-the-decline-in-unemployment-round-1/)
- 2012-10-09 -- [*The Atlantic*](http://www.theatlantic.com/business/archive/2012/10/get-used-to-78-unemployment-why-this-rate-wont-fall-anytime-soon/263395/)
- 2012-10-05 -- [*NYT* Economix](http://economix.blogs.nytimes.com/2012/10/05/forecasting-unemployment/)
- 2012-10-02 -- [*WSJ* Real Time Economics](http://blogs.wsj.com/economics/2012/10/02/new-model-to-predict-unemployment-sees-rate-at-8-1/)
- 2012-09-13 -- [Wonkblog](http://www.washingtonpost.com/blogs/wonkblog/wp/2012/09/13/the-brookings-papers-on-economic-activity-are-out-celebrate-wonkmukkah-by-reading-them/)

### Previous versions

- November 2012 -- [FEDS working paper 2013-19](https://www.federalreserve.gov/econres/feds/the-ins-and-outs-of-forecasting-unemployment-using-labor-force-flows-to-forecast-the-labor-market.htm)