---
title: "Across The Universe: Policy Support for Employment and Revenue in the Pandemic Recession"
authors:
- Ryan A. Decker
- Robert J. Kurtzman
- Byron F. Lutz
- admin

date: "2020-11-30"
doi: "10.17016/FEDS.2020.099"

featured: true

# Schedule page publish date (NOT publication's date).
publishDate: "2020-12-04"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: "*Finance and Economics Discussion Series* 2020-099. Washington: Board of Governors of the Federal Reserve System"
publication_short: "*FEDS* paper"

abstract: "Using data from 14 government sources, we develop comprehensive estimates of U.S. economic activity by sector, legal form of organization, and firm size to characterize how four government direct lending programs---the Paycheck Protection Program, the Main Street Lending Program, the Corporate Credit Facilities, and the Municipal Lending Facilities---relate to these classes of economic activity in the United States. The classes targeted by these programs are vast---accounting for 97 percent of total U.S. employment---though entity-specific financial criteria limit coverage within specific programs. These programs notionally cover a far larger universe than what was targeted by analogous Great Recession-era lending policies. We relate our estimates to those from timely alternative data sources, which do not typically cover the majority of the economic universe."

# Summary. An optional shortened abstract.
summary: We develop comprehensive estimates of U.S. economic activity by sector, legal form of organization, and firm size to characterize how four government direct lending programs relate to these classes of economic activity.

tags:
- labor market
- business cycles
- economic activity
- employment
- Pandemic Recession
- Corporate Credit Facilities
- Main Street
- Paycheck Protection Program
- alternative data
- direct lending
- Federal Reserve

links:
 - name: "FEDS Paper"
   url: "https://www.federalreserve.gov/econres/feds/across-the-universe-policy-support-for-employment-revenue-in-pandemic-recession.htm"

url_pdf: "publication/universe/2020099pap.pdf"
url_code: ""
url_dataset: ""
url_project: ""
url_slides: ""
url_source: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ""
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---

### Presentations
- **Jan. 4, 2021** - AEA Panel Session, ["Comparing the Pandemic Recession to the Great Recession"](https://www.aeaweb.org/conference/2021/preliminary/2092?q=eNpFjb0OwjAQg18l8twBBpauDF0R4gVOqQU35Ee5CFRVeXcCBXWzP8v2CqOZpnhbMjGuf4sRR7QBYpZ8NxhQWUJX5xSyFI13Vx90F4kzg3p3pd-arqZvMhVK3XEfmGX5LWngpp7K1-e15NzB6YDW3iAlL6c,) 
  - [Slides](universe-present-20210104.pdf)
  - [Recording](https://www.aeaweb.org/conference/2021/aea-session-recordings/player?meetingId=235&recordingId=394)
  
<!-- ### Previous versions -->
