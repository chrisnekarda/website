---
title: "Assessing the Change in Labor Market Conditions"
authors:
- Hess T. Chung
- Bruce Fallick
- admin
- David Ratner
date: "2014-12-17"
doi: ""

featured: false

# Schedule page publish date (NOT publication's date).
publishDate: "2014-12-17"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: "*Finance and Economics Discussion Series* 2014-109. Washington: Board of Governors of the Federal Reserve System"
publication_short: "*FEDS* paper"

abstract: This paper describes a dynamic factor model of 19 U.S. labor market indicators, covering the broad categories of unemployment and underemployment, employment, workweeks, wages, vacancies, hiring, layoffs, quits, and surveys of consumers' and businesses' perceptions. The resulting labor market conditions index (LMCI) is a useful tool for gauging the change in labor market conditions. In addition, the model provides a way to organize discussions of the signal value of different labor market indicators in situations when they might be sending diverse signals. The model takes the greatest signal from private payroll employment and the unemployment rate. Other influential indicators include the insured unemployment rate, consumers' perceptions of job availability, and help-wanted advertising. Through the lens of the LMCI, labor market conditions have improved at a moderate pace over the past several years, albeit with some notable variation along the way. In addition, from the perspective of the model, the unemployment rate declined a bit faster over the past two years than was consistent with the other indicators.

# Summary. An optional shortened abstract.
summary: The labor market conditions index (LMCI), a dynamic factor model of 19 monthly indicators, appears to be a useful tool for assessing the change in labor market conditions based on a broad array of information.

tags:
- labor markets
- unemployment
- employment
- unemployment rate
- payroll employment
- dynamic factor model
- LMCI

links:
 - name: "FEDS Paper"
   url: "https://www.federalreserve.gov/econres/feds/assessing-the-change-in-labor-market-conditions.htm"

url_pdf: "publication/lmci-feds/2014109pap.pdf"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: "Average Monthly Change in LMCI"
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---
