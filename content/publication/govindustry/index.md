---
title: "Industry Evidence on the Effects of Government Spending"
authors:
- admin
- vramey
# author_notes:
# - "Equal contribution"
# - "Equal contribution"
date: "2011-01-11"
doi: "10.1257/mac.3.1.36"

featured: false

# Schedule page publish date (NOT publication's date).
publishDate: "2011-01-11"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "[*American Economic Journal: Macroeconomics*](https://www.aeaweb.org/journals/mac), vol. 3, no. 1, January 2011, pp. 36--59"
publication_short: "[*American Economic Journal: Macroeconomics*](https://www.aeaweb.org/journals/mac)"

abstract: This paper investigates the effects of government purchases at the industry level in order to shed light on the transmission mechanism for government spending on the aggregate economy. We create a new panel dataset that matches output and labor variables to industry-specific shifts in government demand. An increase in government demand raises output and hours, lowers real product wages and labor productivity, and has no effect on the markup. The estimates also imply approximately constant returns to scale. The findings are more consistent with the effects of government spending in the neoclassical model than the textbook New Keynesian model.

# Summary. An optional shortened abstract.
summary: An increase in government demand raises output and hours in manufacturing industries, lowers real product wages and labor productivity, and has no effect on the markup.

tags:
- markup
- labor share
- business cycle
- government spending
- productivity
- technology shock

links:
 - name: "Online appendix"
   url: "https://www.aeaweb.org/aej/mac/app/2010-0019_app.zip"

# url_pdf: ''
url_code: ''
url_dataset: 'https://www.openicpsr.org/openicpsr/project/114192'
url_project: ''
url_slides: ''
url_source: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: "Instrumental-Variables Regressions of Markups on Government Demand"
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---

### Previous versions

- [NBER Working Paper 15754](https://www.nber.org/papers/w15754)