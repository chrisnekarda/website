---
title: "Understanding Unemployment Dynamics: The Role of Time Aggregation"
authors:
- admin
date: "2009-06-19"

featured: false

# Schedule page publish date (NOT publication's date).
publishDate: "2009-06-19"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

abstract: "This paper uses weekly data from the Survey of Income and Program Participation (SIPP) to estimate the role of time aggregation in measuring gross labor force flows and unemployment dynamics. Time aggregation is substantial: gross flows estimated from monthly data understate the true number of transitions by 15--24 percent. Time aggregation in both separations to unemployment and accessions from unemployment comoves positively with the business cycle. The effect from time aggregation on separations is roughly offset by its effect on accessions, however, creating no meaningful cyclical bias in measured gross flows or hazard rates. Contrary to claims by Hall (2006) and Shimer (2007), separation hazard rates calculated from the SIPP and the Current Population Survey are strongly countercyclical and remain so after adjusting for time aggregation. In addition, the separation hazard rate contributes fully one-half of the cyclical variance of the steady-state unemployment rate after adjusting for time aggregation."

# Summary. An optional shortened abstract.
summary: Labor market flows estimated from monthly data understate the true number of transitions by 15--25 percent, but this time aggregation bias does not meaningfully affect the cyclicality of gross flows or hazard rates.

tags:
- labor market
- business cycles
- time aggregation bias
- labor market flows
- hiring
- job loss
- CPS
- SIPP

# links:
#  - name: ""
#    url: ""

# url_pdf: "publication/timeagg/timeagg.pdf"
url_code: ""
url_dataset: ""
url_project: ""
url_slides: ""
url_source: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: "Cross-correlations of separation and job finding hazard rates with unemployment rate"
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---

<!-- ### Previous versions -->
