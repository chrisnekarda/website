---
title: "Why is Involuntary Part-Time Work Elevated?"
authors:
- Tomaz Cajner
- Dennis Mawhirter
- admin
- David Ratner
date: "2014-04-14"
doi: "10.17016/2380-7172.0014"

featured: false

# Schedule page publish date (NOT publication's date).
publishDate: "2014-04-14"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated publication name.
publication: "*FEDS Notes*. Washington: Board of Governors of the Federal Reserve System, April 14, 2014."
publication_short: "*FEDS Notes*"

abstract: 'Despite substantial improvement in the unemployment rate and several other labor market indicators, the number of Americans involuntarily working part time (also called "part-time for economic reasons") remains unusually high nearly five years into the recovery. The high level of involuntary part-time work has led to a concern that there is an underbelly of labor market slack not well accounted for by the overall unemployment rate. Our analysis suggests there are reasons to believe that continued cyclical improvement in the labor market will put downward pressure on involuntary part-time work, but there is also a possibility that secular trends may augur structurally higher part-time employment.'

# Summary. An optional shortened abstract.
summary: Continued cyclical improvement in the labor market will put downward pressure on involuntary part-time work, but secular trends may augur structurally higher part-time employment.

tags:
- labor markets
- part-time employment
- hours
- labor market flows
- CPS

links:
 - name: "FEDS Note"
   url: "https://www.federalreserve.gov/econresdata/notes/feds-notes/2014/why-is-involuntary-part-time-work-elevated-20140414.html"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: "Decomposition of yearly change in involuntary part-time work"
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---
