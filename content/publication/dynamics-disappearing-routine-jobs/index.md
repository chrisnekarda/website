---
title: "The Dynamics of Disappearing Routine Jobs: A Flows Approach"
authors:
- Matias Cortes
- Nir Jaimovich
- admin
- Henry E. Siu
# author_notes:
# - "Equal contribution"
# - "Equal contribution"
date: "2020-08-15"
doi: "10.1016/j.labeco.2020.101823"

featured: true

# Schedule page publish date (NOT publication's date).
publishDate: "2020-04-28"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: '[*Labour Economics*](https://www.sciencedirect.com/journal/labour-economics), Special Issue on "Technology and the Labour Market", vol. 65, August 2020, pp. 1018--23'
publication_short: "[*Labour Economics*](https://www.sciencedirect.com/journal/labour-economics)"

abstract: We use matched individual-level CPS data to study the decline in middle-wage routine occupations during the last 40 years, and determine how the associated labor market flows have evolved. The decline in employment in these occupations can be primarily accounted for by changes in transition rates from non-participation and unemployment to routine employment. We study how these transition rates have changed since the mid-1970s, and find that changes are primarily due to the propensity of individuals to make such transitions, whereas relatively little is due to demographic changes. We also find that changes in the propensity to transition into routine occupations account for a substantial proportion of the rise in non-participation observed in the U.S. in recent decades.

# Summary. An optional shortened abstract.
# summary: 

tags:
- labor markets
- worker flows
- polarization
- unemployment
- employment
- hiring
- job loss
- CPS

links:
 - name: "PDF (preprint)"
   url: "micro-macro-post.pdf"

url_pdf: https://www.sciencedirect.com/science/article/pii/S0927537120300270/pdfft?md5=862260f36206fd5bb9626ef9ce05edab&pid=1-s2.0-S0927537120300270-main.pdf
url_code: ''
url_dataset: ''
url_project: ''
url_slides: ''
url_source: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: "Routine employment based on law of motion using monthly rates and phase averages"
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---

- Read our summary of the paper in [*VoxEU*](http://www.voxeu.org/article/disappearance-routine-jobs)

### Media coverage

- 2014-10-09 -- [*The Globe and Mail*](https://faculty.arts.ubc.ca/hsiu/research/globe2014oct.pdf)
- 2014-07-29 -- [FiveThirtyEight](https://fivethirtyeight.com/features/in-the-papers-retail-wages-work-study-school-breakfast-and-job-polarization/)
- 2014-07-25 -- [The Washington Center for Equitable Growth](https://equitablegrowth.org/polarized-future-labor-market/)
- 2014-07-23 -- [engadget](https://www.engadget.com/2014-07-23-technology-and-recession-are-cutting-into-blue-collar-jobs.html)
- 2014-07-22 -- [*New York Times*](https://www.nytimes.com/2014/07/23/upshot/how-technology-aided-by-recession-is-transforming-the-work-world.html)


### Previous versions

 - July 2014 -- "The Micro and Macro of Disappearing Routine Jobs: A Flows Approach," [NBER Working Paper 20307](http://www.nber.org/papers/w20307)
 