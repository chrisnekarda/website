---
title: "Estimating Natural Rates of Unemployment: A Primer"
authors:
- Brandyn Bok
- rcrump
- admin
- npnadeau
# author_notes:
# - "Federal Reserve Bank of New York"
# - ""
# - "Federal Reserve Bank of San Francisco"

date: "2023-08-27"
doi: "10.24148/wp2023-25"

featured: true

# Schedule page publish date (NOT publication's date).
publishDate: "2023-08-27"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: "*Federal Reserve Bank of San Francisco Working Paper* 2023-25."
publication_short: "*FRBSF* working paper"

abstract: "Before the pandemic, the U.S. unemployment rate reached a historic low that was close to estimates of its underlying longer-run value and the short-run level associated with an absence of inflationary pressures. After two turbulent years, unemployment returned to its pre-pandemic low, and the estimated underlying longer-run unemployment rate appeared largely unchanged. However, economic disruptions pushed up the short-run noninflationary rate substantially, as high as 6%. This primer examines these different measures of the natural rate of unemployment and discusses how they can provide useful insights for policymakers."

# Summary. An optional shortened abstract.
# summary: This paper discusses various concepts of unemployment rate benchmarks that are frequently used by policymakers for assessing the current state of the economy as it relates to the pursuit of both price stability and maximum employment.

tags:
- labor market
- monetary policy
- unemployment
- business cycles

links:
#  - name: "FEDS Paper"
#    url: "https://www.federalreserve.gov/econres/feds/unemployment-rate-benchmarks.htm"

url_pdf: "publication/estimating-ustars/wp2023-25.pdf"
url_code: "https://drive.google.com/file/d/1Js6cXijvJesFgW022i_1PMeMuq0FAjwg/view"
url_dataset: ""
url_project: ""
url_slides: ""
url_source: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: ""
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---

<!-- ### Previous versions -->
