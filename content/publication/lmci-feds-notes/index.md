---
title: "Assessing the Change in Labor Market Conditions"
authors:
- Hess T. Chung
- Bruce Fallick
- admin
- David Ratner
date: "2014-05-22"
doi: "10.17016/2380-7172.0019"

featured: false

# Schedule page publish date (NOT publication's date).
publishDate: "2014-05-22"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated publication name.
publication: "*FEDS Notes*. Washington: Board of Governors of the Federal Reserve System, May 22, 2014."
publication_short: "*FEDS Notes*"

abstract: The U.S. labor market is large and multifaceted. Often-cited indicators, such as the unemployment rate or payroll employment, measure a particular dimension of labor market activity, and it is not uncommon for different indicators to send conflicting signals about labor market conditions. Accordingly, analysts typically look at many indicators when attempting to gauge labor market improvement. However, it is often difficult to know how to weigh signals from various indicators. Statistical models can be useful to such efforts because they provide a way to summarize information from several indicators. This Note describes a dynamic factor model of labor market indicators that we have developed recently, which we call the labor market conditions index (LMCI).

# Summary. An optional shortened abstract.
summary: The labor market conditions index (LMCI), a dynamic factor model of 19 monthly indicators, appears to be a useful tool for assessing the change in labor market conditions based on a broad array of information.

tags:
- labor markets
- unemployment
- employment
- unemployment rate
- payroll employment
- dynamic factor model
- LMCI

links:
 - name: "FEDS Note"
   url: "https://www.federalreserve.gov/econresdata/notes/feds-notes/2014/assessing-the-change-in-labor-market-conditions-20140522.html"

url_dataset: "http://www.federalreserve.gov/econresdata/notes/feds-notes/2014/files/lmci_feds.csv"

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: "Average Monthly Change in LMCI since 2007"
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---
