---
title: "Unemployment Rate Benchmarks"
authors:
- rcrump
- admin
- npnadeau
# author_notes:
# - "Federal Reserve Bank of New York"
# - ""
# - "Federal Reserve Bank of San Francisco"

date: "2020-08-27"
doi: "10.17016/FEDS.2020.072"

featured: true

# Schedule page publish date (NOT publication's date).
publishDate: "2020-08-27"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["3"]

# Publication name and optional abbreviated publication name.
publication: "*Finance and Economics Discussion Series* 2020-072. Washington: Board of Governors of the Federal Reserve System"
publication_short: "*FEDS* paper"

abstract: "This paper discusses various concepts of unemployment rate benchmarks that are frequently used by policymakers for assessing the current state of the economy as it relates to the pursuit of both price stability and maximum employment. In particular, we propose two broad categories of unemployment rate benchmarks: (1) a longer-run unemployment rate expected to prevail after adjusting to business cycle shocks and (2) a stable-price unemployment rate tied to inflationary pressures. We describes how various existing measures used as benchmark rates fit within this taxonomy with the goal of facilitating the use of a common set of terms for assessments of the current state of the economy and deliberations among policymakers."

# Summary. An optional shortened abstract.
summary: This paper discusses various concepts of unemployment rate benchmarks that are frequently used by policymakers for assessing the current state of the economy as it relates to the pursuit of both price stability and maximum employment.

tags:
- labor market
- monetary policy
- unemployment
- business cycles


links:
 - name: "FEDS Paper"
   url: "https://www.federalreserve.gov/econres/feds/unemployment-rate-benchmarks.htm"

url_pdf: "publication/unemp-rate-benchmarks/2020072pap.pdf"
url_code: ""
url_dataset: ""
url_project: ""
url_slides: ""
url_source: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ""
  focal_point: "Top"
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: []
---

<!-- ### Previous versions -->
