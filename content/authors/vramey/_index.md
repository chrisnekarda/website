---
# Display name
title: Valerie A. Ramey

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Professor of Economics

# Organizations/Affiliations to show in About widget
organizations:
- name: University of California, San Diego
  url: https://econweb.ucsd.edu/~vramey/

# Short bio (displayed in user profile at end of posts)
bio:

# Interests to show in About widget
interests:
- Macroeconomics
- Fiscal policy
- Time use
- Economic fluctuations
- Inventories

# Education to show in About widget
education:
  courses:
  - course: Ph.D., Economics
    institution: Stanford University
    year: 1987
  - course:  B.A., Economics and Spanish
    institution: University of Arizona
    year: 1981
  
# Social/Academic Networking
social:
- icon: envelope
  icon_pack: far
  link: "mailto:vramey@ucsd.edu"
- icon: bookmark
  icon_pack: far
  link: https://econweb.ucsd.edu/~vramey/
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=z5_o_w4AAAAJ

# Email for Contact widget or Gravatar
email: "vramey@ucsd.edu"

---

[Official website](https://econweb.ucsd.edu/~vramey/)

Valerie Ramey received her B.A. with a double major in Economics and Spanish from the University of Arizona, graduating summa cum laude. She went on to earn a Ph.D. in Economics from Stanford University. She is currently a Professor of Economics at the University of California, San Diego and a Research Associate of the National Bureau of Economic Research. She is a member of the American Academy of Arts and Sciences and a Fellow of the Econometric Society. She has served as co-editor of the *American Economic Review*, chair of the Economics Department at UCSD, and as a member of several National Science Foundation Advisory Panels and the Federal Economic Statistics Advisory Committee. She currently serves on the Panel of Economic Advisers for the Congressional Budget Office and on the NBER Business Cycle Dating Committee, and she is an associate editor of the *Quarterly Journal of Economics* and the *Journal of Political Economy*.

Professor Ramey has published numerous scholarly articles and policy-relevant articles on the sources of business cycles, trends in wage inequality, the effects of monetary and fiscal policy, the impact of volatility on growth, and various topics on time use, such as the increase in time investments in children by educated parents. Her recent work has studied the size of government spending multipliers during recessions and periods of low interest rates. She has received research grants from the National Science Foundation, the Alfred P. Sloan Foundation, and the Bradley Foundation.
