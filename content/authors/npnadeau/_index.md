---
# Display name
title: Nicolas Petrosky-Nadeau

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Vice President for Macroeconomic Research

# Organizations/Affiliations to show in About widget
organizations:
- name: Federal Reserve Bank of San Francisco
  url: https://www.frbsf.org/

# Short bio (displayed in user profile at end of posts)
bio: Nicolas is a Vice President at the San Francisco Fed

# Interests to show in About widget
interests:
- Macroeconomics
- Labor
- Business cycles

# Education to show in About widget
education:
  courses:
  - course: Ph.D., Economics
    institution: University of Quebec (Montreal) and Sciences Po (Paris)
    year: 2009
  - course: M.A., Economics
    institution: Institut des HEI, Geneva
    year: 2004
  - course: B.A., Economics
    institution: Ecole des HEC, Montreal
    year: 2001
  
# Social/Academic Networking
social:
- icon: envelope
  icon_pack: far
  link: "mailto:nicolas.petrosky-nadeau@sf.frb.org"
- icon: bookmark
  icon_pack: far
  link: https://www.frbsf.org/economic-research/economists/nicolas-petrosky-nadeau/
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=NTFdQh8AAAAJ

# Email for Contact widget or Gravatar
email: "nicolas.petrosky-nadeau@sf.frb.org"

---

[Official Website](https://www.frbsf.org/economic-research/economists/nicolas-petrosky-nadeau/)

[Personal Website](https://sites.google.com/view/nicolas-petrosky-nadeau/home)

Nicolas' main area of research is understanding the changes in aggregate labor markets in the longer run and over the business cycle. Specifically, he uses search and matching models to study variations in unemployment, job vacancies, and wages, and consider the implications of introducing frictions in products and credit markets. Currently he is at the Federal Reserve Bank of San Francisco as Vice President for Macroeconomic Research, and formerly on the faculty of the Tepper School of Business at Carnegie Mellon University.
