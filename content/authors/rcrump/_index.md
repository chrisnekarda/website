---
# Display name
title: Richard K. Crump

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Vice President for Capital Markets Function

# Organizations/Affiliations to show in About widget
organizations:
- name: Federal Reserve Bank of New York
  url: https://www.newyorkfed.org/

# Short bio (displayed in user profile at end of posts)
bio: Richard is a Vice President for Capital Markets Function at the New York Fed

# Interests to show in About widget
interests:
- Econometric theory
- Financial economics

# Education to show in About widget
education:
  courses:
  - course: Ph.D., Economics
    institution: University of California, Berkeley
    year: 2009
  - course:  M.A., Statistics
    institution: University of California, Berkeley
    year: 2006
  - course:  B.S., Economics
    institution: Massachusetts Institute of Technology
    year: 2000
  
# Social/Academic Networking
social:
- icon: envelope
  icon_pack: far
  link: "mailto:richard.crump@ny.frb.org"
- icon: bookmark
  icon_pack: far
  link: https://www.newyorkfed.org/research/economists/crump/index.html
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=

# Email for Contact widget or Gravatar
email: "richard.crump@ny.frb.org"

---

[Official Website](https://www.newyorkfed.org/research/economists/crump/index.html)

Richard Crump joined the Federal Reserve Bank of New York in 2009. His research interests are in Econometric Theory and Financial Economics. He holds a Ph.D. in Economics and an M.A. in Statistics from the University of California at Berkeley, along with a B.S. in Economics from MIT. Prior to graduate school he worked as an Associate in the US Economic Research Group and the Global Markets Research Group at Goldman Sachs.
