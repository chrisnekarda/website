---
# Display name
title: Regis Barnichon

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Senior Research Advisor

# Organizations/Affiliations to show in About widget
organizations:
- name: Federal Reserve Bank of San Francisco
  url: https://www.frbsf.org/

# Short bio (displayed in user profile at end of posts)
bio: Regis is a Senior Research Advisor at the San Francisco Fed

# Interests to show in About widget
interests:
- Macroeconomics
- Applied econometrics
- Labor

# Education to show in About widget
education:
  courses:
  - course: Ph.D., Economics
    institution: London School of Economics, UK
    year: 2007
  - course:  BSc, Physics and Mathematics
    institution: Ecole Polytechnique, France
    year: 2002
  
# Social/Academic Networking
social:
- icon: envelope
  icon_pack: far
  link: "mailto:regis.barnichon@sf.frb.org"
- icon: bookmark
  icon_pack: far
  link: https://www.frbsf.org/economic-research/economists/regis-barnichon/
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=LwiMQfAAAAAJ

# Email for Contact widget or Gravatar
email: "regis.barnichon@sf.frb.org"

---

[Official Website](https://www.frbsf.org/economic-research/economists/regis-barnichon/)

[Personal Website](http://sites.google.com/site/regisbarnichon)

Regis is a Senior Research Advisor at the San Francisco Fed