---
# Display name
title: Garey Ramey

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Professor of Economics

# Organizations/Affiliations to show in About widget
organizations:
- name: University of California, San Diego
  url: https://econweb.ucsd.edu/~gramey/

# Short bio (displayed in user profile at end of posts)
bio:

# Interests to show in About widget
interests:

# Education to show in About widget
education:
  courses:
  - course: Ph.D., Economics
    institution: Stanford University
    year: 1987
  - course:  B.A., Economics
    institution: University of Arizona
    year: 1980
  
# Social/Academic Networking
social:
- icon: envelope
  icon_pack: far
  link: "mailto:gramey@ucsd.edu"
- icon: bookmark
  icon_pack: far
  link: https://econweb.ucsd.edu/~gramey/
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=QdGMrlYAAAAJ

# Email for Contact widget or Gravatar
email: "gramey@ucsd.edu"

---

[Official Website](https://econweb.ucsd.edu/~gramey/)