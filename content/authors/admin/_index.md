---
# Display name
title: Christopher J. Nekarda

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Principal Economist

# Organizations/Affiliations to show in About widget
organizations:
- name: Board of Governors of the Federal Reserve System
  url: https://federalreserve.gov

# Short bio (displayed in user profile at end of posts)
bio: Any views expressed on this site are my own and do not necessarily represent the views or policies of the Board of Governors of the Federal Reserve System or its staff.

# Interests to show in About widget
interests:
- Macroeconomics
- Labor economics
- Applied econometrics

# Education to show in About widget
education:
  courses:
  - course: Ph.D., Economics
    institution: University of California, San Diego
    year: 2008
  - course:  BA, Economics
    institution: University of California, Berkeley
    year: 2000
  
# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
social:
- icon: cv
  icon_pack: ai
  link: files/nekarda-cv.pdf
- icon: envelope
  icon_pack: far
  link: '/#contact'  # For a direct email link, use "mailto:email.address@domain.tld".
- icon: mastodon
  icon_pack: fab
  link: https://econtwitter.net/@chrisnekarda
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=YRUdpt4AAAAJ
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0002-2301-2720
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/chrisnekarda


# Email for Contact widget or Gravatar
email: ""

---

I am an economist at the Federal Reserve Board. My research focuses on U.S.&nbsp;labor markets, macroeconomics, and their trends and cyclical interaction. I am interested in questions about labor market polarization, labor market flows, forecasting, as well as studying current labor market developments.

This my personal website. Any views expressed on this site are my own and do not necessarily represent the views or policies of the Board of Governors of the Federal Reserve System or its staff. My official website is [here](https://www.federalreserve.gov/econres/christopher-j-nekarda.htm).
