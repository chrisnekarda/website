---
# Display name
title: Shegiru Fujita

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: Economic Advisor and Economist

# Organizations/Affiliations to show in About widget
organizations:
- name: Federal Reserve Bank of Philadelphia
  url: https://www.phil.frb.org/

# Short bio (displayed in user profile at end of posts)
bio:

# Interests to show in About widget
interests:
- Macroeconomics
- Labor economics
- Applied time-series econometrics

# Education to show in About widget
education:
  courses:
  - course: Ph.D., Economics
    institution: University of California, San Diego
    year: 2004
  - course:  B.A., Economics
    institution: Yokohama National University
    year: 1993
  
# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: far
  link: "mailto:shigeru.fujita@phil.frb.org"
- icon: bookmark
  icon_pack: far
  link: https://www.phil.frb.org/research-and-data/economists/fujita/
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=QdGMrlYAAAAJ

# Email for Contact widget or Gravatar
email: "shigeru.fujita@phil.frb.org"

---

[Official website](https://www.phil.frb.org/research-and-data/economists/fujita/)