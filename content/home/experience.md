---
# An instance of the Experience widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: experience

# Activate this widget? true/false
active: false

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 40

title: Experience
subtitle:

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format: Jan 2006

# Experiences.
#   Add/remove as many `experience` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   You can begin a multiline `description` using YAML's `|-`.
experience:
- company: Board of Governors of the Federal Reserve System
  company_url: "https://www.federalreserve.gov"
  date_end: ""
  date_start: "2009-04-15"
#   description: |-
#     Responsibilities include:
#     * Analysing
#     * Modelling
#     * Deploying
  location: Washington, DC
  title: Pricipal Economist

- company: Brookings Institution
  company_url: "https://www.brookings.edu"
  date_end: "2002-08-15"
  date_start: "2000-08-15"
#   description: 
  location: Washington, DC
  title: Research Assistant
---
