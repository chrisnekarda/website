#!/bin/bash
# call this script from main directory (e.g., `% ./scripts/build_site.sh`)

# remove old public/
rm -rf public

# build site
hugo -b "https://chrisnekarda.com/"

# change permissions to make www friendly
find public -type f -exec chmod 664 {} \;
find public -type d -exec chmod 775 {} \;
