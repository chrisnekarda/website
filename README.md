# Christopher J. Nekarda professional website

## Initialization

```bash
#!/bin/bash

# First, create empty project in GitLab. Then clone it to local workspace.
git clone git@gitlab.com:chrisnekarda/website.git
cd website

# Add a secondary remote called upstream
git remote add upstream https://github.com/wowchemy/starter-academic.git

# Fetch the remote and then pull its changes into your local master branch.
git checkout master
git fetch upstream
git pull upstream master

# Push to your own remote origin to keep the forked repo in sync
git push origin master

# Create develop branch and work there
git checkout -b develop
```
